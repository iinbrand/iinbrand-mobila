# iinbrand Mobila

Run build
```
$ grunt build
```

Start the server and preview your app
```
$ grunt serve
```

The `server` task has been deprecated. Use `grunt serve` to start a server.
```
$ grunt server
```

Open browser

http://localhost:9000
